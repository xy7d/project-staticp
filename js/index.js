//退出
document.querySelector('#logout').addEventListener('click', function () {
    localStorage.removeItem('token')
    location.href = '/login.html'
})


//把用户的信息渲染展示在页面上
const userName = JSON.parse(localStorage.getItem('token'))
// console.log(userName);
document.querySelector('#userName').innerHTML = userName.username
document.querySelectorAll('.avatar img').forEach(item => {
    item.src = userName.avatar ? userName.avatar : 'https://yanxuan-item.nosdn.127.net/8b27deb1670c53e67c42ca3e1ed6fd12.jpg'
})

// rem适配
function fn() {
    // 当屏幕宽度 <= 1024时，适配1024
    // 当屏幕宽度 >= 1920时，适配1920
    let w = window.innerWidth
    if (w <= 1024) w = 1024
    if (w >= 1920) w = 1920
    document.documentElement.style.fontSize = w / 80 + 'px'
}
//浏览器视口大小发生改变的事件，页面改变，计算fontsize大小
window.addEventListener('resize', fn)
let groupDataObj
axios.get('/dashboard')
    .then(({ data: { data: { year, salaryData, provinceData, overview
        , groupData } } }) => {
        // console.log(data);
        // console.log(groupData);
        render(overview)
        line(year)
        salary(salaryData)
        lines(groupData)
        map(provinceData)
        groupDataObj = groupData
    }).catch(err => {
        // console.dir(err)
    })

//todo  学生信息渲染
function render(overview) {
    const { salary, student_count, age, group_count } = overview
    document.querySelector('[name="salary"]').innerHTML = salary
    document.querySelector('[name="student_count"]').innerHTML = student_count
    document.querySelector('[name="age"]').innerHTML = age
    document.querySelector('[name="group_count"]').innerHTML = group_count
}
//todo  线状图
function line(year) {
    let monthArr = []
    let salaryArr = []
    year.forEach(({ month, salary }) => {
        monthArr.push(month)
        salaryArr.push(salary)
    })
    // console.log(monthArr, salaryArr);
    const myEchart = echarts.init(document.querySelector('#line'))
    // 指定图表的配置项和数据
    const option = {
        title: {
            text: '2021全学科薪资走势',
            left: 10,
            top: 15,
            textStyle: {
                fontSize: 16,
            }
        },
        xAxis: {
            type: 'category',
            data: monthArr,
            axisLine: {
                show: true,
                lineStyle: {
                    color: 'green',
                    type: 'dashed'
                }
            },
            axisLabel: {
                show: true,
                color: 'red'
            }
        },
        yAxis: {
            type: 'value',
            splitLine: {
                lineStyle: {
                    type: 'dashed',
                    color: ['#ccc']
                }
            }
        },
        tooltip: {
            show: true,
            trigger: 'axis',
        },
        // 刻度
        grid: {
            left: '10%',
            top: '20%',
        },
        // 折线颜色
        color: [
            {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 1,
                y2: 1,
                colorStops: [
                    {
                        offset: 0,
                        color: '#599bff', // 0% 处的颜色
                    },
                    {
                        offset: 1,
                        color: '#599bff', // 100% 处的颜色
                    },
                ],
            },
        ],
        series: [
            {
                data: salaryArr,
                type: 'line',
                smooth: true,
                symbol: 'emptyCircle',
                symbolSize: 10,
                lineStyle: {
                    width: 5,
                },
                // 区域颜色
                areaStyle: {
                    color: {
                        type: 'linear',
                        x: 0,
                        y: 0,
                        x2: 0,
                        y2: 1,
                        colorStops: [
                            {
                                offset: 0,
                                color: '#86bff3', // 0% 处的颜色
                            },
                            {
                                offset: 0.8,
                                color: 'rgba(255,255,255,0.1)', // 0% 处的颜色
                            },
                            {
                                offset: 1,
                                color: 'rgba(255,255,255,0)', // 100% 处的颜色
                            },
                        ],
                    },
                },
            },
        ],
    }
    myEchart.setOption(option)
    //图表的适配效果
    window.addEventListener('resize', () => {
        // echarts提供的resize方法
        myEchart.resize()
    })
}

//todo  饼状图
function salary(salaryData) {
    let salaryDataArr = []
    let salaryDataGirlArr = []
    let salaryDataBoyArr = []
    salaryData.forEach(({ label, g_count, b_count }) => {
        salaryDataArr.push({ value: g_count + b_count, name: label })
        salaryDataGirlArr.push({ value: g_count, name: label })
        salaryDataBoyArr.push({ value: b_count, name: label })
    })
    gender(salaryDataGirlArr, salaryDataBoyArr)
    // console.log(salaryDataArr);
    const myEchart = echarts.init(document.querySelector('#salary'))
    const option = {
        title: {
            text: '班级薪资分布',
            top: 15,
            left: 10,
            textStyle: {
                fontSize: 16,
            },
        },
        tooltip: {
            trigger: 'item',
        },
        legend: {
            bottom: '6%',
            left: 'center',
        },
        color: ['#FDA224', '#5097FF', '#3ABCFA', '#34D39A', '#ee6666'],
        series: [
            {
                name: '班级薪资分布',
                type: 'pie',
                radius: ['50%', '64%'], // 圆的半径
                center: ['50%', '45%'], // 圆的中心点坐标
                itemStyle: {
                    borderRadius: 10,
                    borderColor: '#fff',
                    borderWidth: 2,
                },
                label: {
                    show: false, // 默认不显示数据项name
                    position: 'center',
                },
                labelLine: {
                    show: false, // 刻度线，不显示
                },
                data: salaryDataArr
            },
        ],
    }
    myEchart.setOption(option)
    //图表的适配效果
    window.addEventListener('resize', () => {
        // echarts提供的resize方法
        myEchart.resize()
    })
}

//todo 男女薪资分布饼图
function gender(salaryDataGirlArr, salaryDataBoyArr) {
    const myEchart = echarts.init(document.querySelector('#gender'))
    const option = {
        title: [
            {
                text: '男女薪资分布',
                left: 10,
                top: 10,
                textStyle: {
                    fontSize: 16,
                },
            },
            {
                text: '男生',
                left: '50%',
                top: '45%',
                textAlign: 'center',
                textStyle: {
                    fontSize: 12,
                },
            },
            {
                text: '女生',
                left: '50%',
                top: '85%',
                textAlign: 'center',
                textStyle: {
                    fontSize: 12,
                },
            },
        ],
        color: ['#FDA224', '#5097FF', '#3ABCFA', '#34D39A'],
        tooltip: {
            trigger: 'item',
        },
        series: [
            {
                type: 'pie',
                radius: ['20%', '30%'],
                center: ['50%', '30%'],
                // data: [{name:'1万以下',value: 4},...]
                data: salaryDataBoyArr
            },
            {
                type: 'pie',
                radius: ['20%', '30%'],
                center: ['50%', '70%'],
                // data: [{name:'1万以下',value: 4},...]
                data: salaryDataGirlArr
            },
        ],
    }
    myEchart.setOption(option)

    //图表的适配效果
    window.addEventListener('resize', () => {
        // echarts提供的resize方法
        myEchart.resize()
    })
}


//todo  柱状图
function lines(groupData, index = 1) {
    let nameArr = []
    let hope_salaryArr = []
    let salaryArr = []
    groupData[index].forEach(({ name, hope_salary, salary }) => {
        nameArr.push(name)
        hope_salaryArr.push(hope_salary)
        salaryArr.push(salary)
    })
    const myEchart = echarts.init(document.querySelector('#lines'))
    const option = {
        grid: {
            left: 70,
            top: 30,
            right: 30,
            bottom: 50,
        },
        xAxis: {
            type: 'category',
            data: nameArr,
            axisLine: {
                lineStyle: {
                    color: '#ccc',
                    type: 'dashed',
                },
            },
            axisLabel: {
                color: '#999',
            },
        },
        yAxis: {
            type: 'value',
            splitLine: {
                lineStyle: {
                    type: 'dashed',
                },
            },
        },
        tooltip: {
            trigger: 'item',
        },
        color: [
            {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [
                    {
                        offset: 0,
                        color: '#34D39A', // 0% 处的颜色
                    },
                    {
                        offset: 1,
                        color: 'rgba(52,211,154,0.2)', // 100% 处的颜色
                    },
                ],
            },
            {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [
                    {
                        offset: 0,
                        color: '#499FEE', // 0% 处的颜色
                    },
                    {
                        offset: 1,
                        color: 'rgba(73,159,238,0.2)', // 100% 处的颜色
                    },
                ],
            },
        ],
        series: [
            {
                data: hope_salaryArr,
                type: 'bar',
                name: '期望薪资',
            },
            {
                data: salaryArr,
                type: 'bar',
                name: '就业薪资',
            },
        ],
    };

    // 使用刚指定的配置项和数据显示图表。
    myEchart.setOption(option)
    //图表的适配效果
    window.addEventListener('resize', () => {
        // echarts提供的resize方法
        myEchart.resize()
    })
}

//todo 小组切换，柱状图切换
document.querySelector('#btns').addEventListener('click', function (e) {
    if (e.target.tagName === 'BUTTON') {
        //高亮
        document.querySelector('.btn-blue').classList.remove('btn-blue')
        e.target.classList.add('btn-blue')
        // console.log(e.target.innerHTML);
        const index = e.target.innerHTML
        lines(groupDataObj, index)
    }
})

//todo  地图
function map(provinceData) {
    const myEchart = echarts.init(document.querySelector('#map'))
    const dataList = [
        { name: '南海诸岛', value: 0 },
        { name: '北京', value: 0 },
        { name: '天津', value: 0 },
        { name: '上海', value: 0 },
        { name: '重庆', value: 0 },
        { name: '河北', value: 0 },
        { name: '河南', value: 0 },
        { name: '云南', value: 0 },
        { name: '辽宁', value: 0 },
        { name: '黑龙江', value: 0 },
        { name: '湖南', value: 0 },
        { name: '安徽', value: 0 },
        { name: '山东', value: 0 },
        { name: '新疆', value: 0 },
        { name: '江苏', value: 0 },
        { name: '浙江', value: 0 },
        { name: '江西', value: 0 },
        { name: '湖北', value: 0 },
        { name: '广西', value: 0 },
        { name: '甘肃', value: 0 },
        { name: '山西', value: 0 },
        { name: '内蒙古', value: 0 },
        { name: '陕西', value: 0 },
        { name: '吉林', value: 0 },
        { name: '福建', value: 0 },
        { name: '贵州', value: 0 },
        { name: '广东', value: 0 },
        { name: '青海', value: 0 },
        { name: '西藏', value: 0 },
        { name: '四川', value: 0 },
        { name: '宁夏', value: 0 },
        { name: '海南', value: 0 },
        { name: '台湾', value: 0 },
        { name: '香港', value: 0 },
        { name: '澳门', value: 0 },
    ]
    dataList.forEach((item) => {
        const obj = provinceData.find((it) => it.name.replace(/省|回族自治区|吾尔自治区|壮族自治区|特别行政区|自治区/g, '') === item.name)
        if (obj) item.value = obj.value
    })
    let option = {
        title: {
            text: '籍贯分布',
            top: 10,
            left: 10,
            textStyle: {
                fontSize: 16,
            },
        },
        tooltip: {
            trigger: 'item',
            formatter: '{b}: {c} 位学员',
            borderColor: 'transparent',
            backgroundColor: 'rgba(0,0,0,0.5)',
            textStyle: {
                color: '#fff',
            },
        },
        visualMap: {
            min: 0,
            max: 6,
            left: 'left',
            bottom: '20',
            text: ['6', '0'],
            inRange: {
                color: ['#ffffff', '#0075F0'],
            },
            show: true,
            left: 40,
        },
        geo: {
            map: 'china',
            roam: false,
            zoom: 1.0,
            label: {
                normal: {
                    show: true,
                    fontSize: '10',
                    color: 'rgba(0,0,0,0.7)',
                },
            },
            itemStyle: {
                normal: {
                    borderColor: 'rgba(0, 0, 0, 0.2)',
                    color: '#e0ffff',
                },
                emphasis: {
                    areaColor: '#34D39A',
                    shadowOffsetX: 0,
                    shadowOffsetY: 0,
                    shadowBlur: 20,
                    borderWidth: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)',
                },
            },
        },
        series: [
            {
                name: '籍贯分布',
                type: 'map',
                geoIndex: 0,
                data: dataList,
            },
        ],
    }
    myEchart.setOption(option)
    //图表的适配效果
    window.addEventListener('resize', () => {
        // echarts提供的resize方法
        myEchart.resize()
    })
}