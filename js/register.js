
// 注册账号
const form = document.querySelector('form')
document.querySelector('#btn-register').addEventListener('click', function () {
    const fd = new FormData(form)
    axios.post('/register', fd)
        .then(({ data: { message } }) => {
            // console.log(message)
            tip(`${message}，请前往登录`)
            location.href = '/login.html'
            form.reset()
        })
        .catch(({ response: { data: { detail } } }) => {
            // console.dir(me)
            // console.log(detail[0].message);
            tip(detail[0].message)
        })
})
