
// 登录账号

const form = document.querySelector('form')
document.querySelector('#btn-login').addEventListener('click', function () {
    const fd = new FormData(form)
    axios.post('/login', fd)
        .then(({ data: { data: { token, username, avatar }, message } }) => {
            // console.log(data)
            let userObj = JSON.stringify({ token, username, avatar })
            if (!userObj) return
            localStorage.setItem('token', userObj)
            tip(`${message}，首页正在骑马来的路上`)
            location.href = '/index.html'
            form.reset()
        })
        .catch(({ response: { data: { message } } }) => {
            // console.dir(err)
            // console.log(message);
            tip(message)
        })
})
