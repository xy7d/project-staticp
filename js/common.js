// 上面这个代码处理过度动画（默认加上不用管）
document.addEventListener('DOMContentLoaded', () => {
  setTimeout(() => {
    document.body.classList.add('sidenav-pinned')
    document.body.classList.add('ready')
  }, 200)
})

axios.defaults.baseURL = ' http://ajax-api.itheima.net/'


// bootstrap轻提示
const toastBox = document.querySelector('#myToast')
const toast = new bootstrap.Toast(toastBox, {
  animation: true, // 开启过渡动画
  autohide: true, // 开启自动隐藏
  delay: 3000 // 3000ms后自动隐藏
})
const tip = (msg) => {
  toastBox.querySelector('.toast-body').innerHTML = msg
  toast.show()
}

//请求拦截器
axios.interceptors.request.use(config => {
  // Do something before request is sent
  if (localStorage.getItem('token')) {
    config.headers.Authorization = JSON.parse(localStorage.getItem('token')).token
  }
  return config;
}, error => {
  // Do something with request error
  return Promise.reject(error);
});

// 响应拦截器
axios.interceptors.response.use(response => {
  // Do something before response is sent
  // console.log(response.data.message);
  return response;
}, error => {
  // Do something with response error
  console.dir(error);
  if (error.response.data.message === 'token验证失败') {
    location.href = '/login.html'
  }
  return Promise.reject(error);
});





